# Changelog MedReg Accounting Unlimited

* Added   : untuk penambahan fitur.
* Changed : untuk perubahan pada menu yang sudah ada atau optimasi.
* Removed : untuk menghapus fitur.
* Fixed   : untuk perbaikan bug atau error.

# [4.3.0.5] - 2018-05-15
## Menu Registrasi Pasien
- **Added :**
	1. Cetak kartu pasien ada 3 jenis, A4, Kartu Portrait, Kartu Landscape.
	
# [4.3.0.4] - 2018-02-27
## Menu Backup Database
- **Changed :**
	1. Optimasi Sistem Backup.
## Menu Registrasi Pasien
- **Added :**
	1. Filter tanggal registrasi per 1 bulan.
	2. Bisa menghapus pasien yang belum pernah melakukan reservasi.
- **Changed :**
	1. Pencarian data pasien menggunakan tombol Cari.
	2. Optimasi sistem registrasi pasien.

# [4.3.0.3] - 2017-12-11
## Menu Utama
- **Fixed** 
	1. Fix label Nama dan Alamat klinik bisa lebih dari 1 baris (Auto-WordWrap).